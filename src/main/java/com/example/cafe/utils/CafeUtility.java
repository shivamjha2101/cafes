package com.example.cafe.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class CafeUtility {
	public CafeUtility() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static ResponseEntity<String> getResponseEntity(String responseMessage, HttpStatus status) {
		return new ResponseEntity<String>("{\"message\":\"" + responseMessage + "\"}", status);
	}

	public static String getUUid() {
		Date date = new Date();
		long time = date.getTime();
		return "Bill-" + time;
	}

	public static JSONArray getJsonArrayFromString(String data) throws JSONException {
		JSONArray jsonArray = new JSONArray(data);
		return jsonArray;
	}

	public static Map<String, Object> getMapFromJson(String data) {
		if (!Strings.isNullOrEmpty(data)) {
			return new Gson().fromJson(data, new TypeToken<Map<String, Object>>() {
			}.getType());

		}
		return new HashMap<>();
	}
}
