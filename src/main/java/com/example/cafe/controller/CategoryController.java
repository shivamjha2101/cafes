package com.example.cafe.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.cafe.constents.UserConstents;
import com.example.cafe.model.Category;
import com.example.cafe.service.CategoryService;
import com.example.cafe.utils.CafeUtility;

@RestController
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@PostMapping("/add")
	public ResponseEntity<String> addNewCategory(@RequestBody Category category) {
		try {

			return categoryService.addNewCategory(category);

		} catch (Exception e) {
			return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/get")
	public ResponseEntity<List<Object>> getAllCategory() {
		try {
			return categoryService.getAllCategory();
		} catch (Exception e) {
			return new ResponseEntity<List<Object>>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PostMapping("/update")
	public ResponseEntity<String> updateCategory(@RequestBody Category category) {
		try {

			return categoryService.updateCategory(category);

		} catch (Exception e) {
			return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
