package com.example.cafe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.cafe.constents.UserConstents;
import com.example.cafe.model.Bill;
import com.example.cafe.service.BillService;
import com.example.cafe.utils.CafeUtility;

@RestController
@RequestMapping("/bill")
public class BillController {

	@Autowired
	private BillService billService;

	@PostMapping("/generateReport")
	ResponseEntity<String> generateReport(@RequestBody Bill bill)
	{
		try {
			return billService.generateReport(bill);
		}catch(Exception e)
		{
			return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
