package com.example.cafe.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.cafe.constents.UserConstents;
import com.example.cafe.dto.ProductDto;
import com.example.cafe.model.Product;
import com.example.cafe.service.ProductService;
import com.example.cafe.utils.CafeUtility;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService productService;

	@PostMapping("/add")
	public ResponseEntity<String> addNewProduct(@RequestBody Product product) {
		try {
			return productService.addProduct(product);
		} catch (Exception e) {
			return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/get")
	public ResponseEntity<List<ProductDto>> getAllProduct() {
		try {
			return productService.getAllProduct();
		} catch (Exception e) {
			return new ResponseEntity<List<ProductDto>>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/update")
	public ResponseEntity<String> updateProduct(@RequestBody Product product) {
		try {
			return productService.updateProduct(product);
		} catch (Exception e) {
			return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/delete/{id}")
	public ResponseEntity<String> deleteProduct(@PathVariable long id) {
		try {
			return productService.deleteProduct(id);
		} catch (Exception e) {
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
//	@GetMapping("/getByCategory")
//	public ResponseEntity<String> getByCategory(@PathVariable long id) {
//		try {
//			return productService.deleteProduct(id);
//		} catch (Exception e) {
//			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
	
}
