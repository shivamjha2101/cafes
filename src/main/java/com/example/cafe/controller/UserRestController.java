package com.example.cafe.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.cafe.constents.UserConstents;
import com.example.cafe.model.UserData;
import com.example.cafe.service.UserService;
import com.example.cafe.utils.CafeUtility;

@RestController
@RequestMapping("/user")
public class UserRestController {

	@Autowired
	private UserService userService;

	@PostMapping(path = "/signup")
	public ResponseEntity<String> signUp(@RequestBody UserData user) {
		try {
			return userService.signUp(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(path = "/login")
	public ResponseEntity<String> login(@RequestParam("username") String user,
			@RequestParam("password") String password) {
		// TODO Auto-generated method stub
		try {
			return userService.login(user, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(path = "/get")
	public ResponseEntity<List<Object>> getAllUser() {
		try {
			return userService.getAllUser();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<List<Object>>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(path = "/update")
	public ResponseEntity<String> update(@RequestBody(required = true) UserData userData) {
		try {
			return userService.update(userData);
		} catch (Exception ex) {
			return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PostMapping(path = "/forgotPassword")
	ResponseEntity<String> forgotPassword(@RequestParam("email") String email) {
		try {
			userService.forgotPassword(email);
			return CafeUtility.getResponseEntity("Check your mail for credentials", HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
