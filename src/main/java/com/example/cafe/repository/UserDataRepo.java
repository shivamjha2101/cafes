package com.example.cafe.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.cafe.model.UserData;

@Repository
public interface UserDataRepo extends JpaRepository<UserData, Long> {
	public UserData findByEmail(String email);

	@Query(value = "select name,email,contact_number,role from user_data where role='user'", nativeQuery = true)
	public List<Object> getAllUser();


	@Transactional
	@Modifying
	@Query(value = "update user_data SET status = ?1 where id = ?2", nativeQuery = true)
	public String updateStatus(String status,  long id);
}
