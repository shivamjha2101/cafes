package com.example.cafe.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.cafe.model.Category;

@Repository
public interface CategoryRepo extends JpaRepository<Category, Long> {

	@Query(value = "select * from category", nativeQuery = true)
	public List<Object> getAllCategory();
}
