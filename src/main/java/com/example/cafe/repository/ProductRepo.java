package com.example.cafe.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.cafe.dto.ProductDto;
import com.example.cafe.model.Product;

public interface ProductRepo extends JpaRepository<Product, Long> {

	public List<ProductDto> getAllProduct();
}
