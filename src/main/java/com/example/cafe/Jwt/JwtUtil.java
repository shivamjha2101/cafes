package com.example.cafe.Jwt;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JwtUtil {

	private String secret = "shivamjha";

	// ******----- Here we validate the token ------******

	// Returns the username which is extracted from token
	// Uses extractClaims method as processing the username
	public String extractUsername(String token) {
		return extractClaims(token, Claims::getSubject);
	}

	// Returns the expiration time of token
	// Uses extractClaims method as processing the extractionDate
	public Date extractExpiration(String token) {
		return extractClaims(token, Claims::getExpiration);
	}

	// This method is for extracting all claims which is present in token and it
	// returns the result in any format (T) It uses claim resolver to return the
	// claims in user desired format (E.g)->Username,expirationDate
	// Uses ExractAllClaims function to extract all claims which present in token
	public <T> T extractClaims(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = extractAllClaims(token);
		return claimsResolver.apply(claims);
	}

	// This method is for extracting all claims which is present in token and it
	// returns the result in claim format
	public Claims extractAllClaims(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}

	// Comparing the expiration time with current time wheather the token is expired
	// or not Uses extractExpiration for process the tokenExpired or not
	// Returns the bool value
	private boolean isTokenExpired(String token) {
		return extractExpiration(token).before(new Date());
	}

	// Validate the token weather it is valid or not-valid
	// Uses extractUsername function and isTokenExpired function
	// Compares the username which is extracted from token with real user and check
	// wheather tokenisexpired or not
	public boolean validateToken(String token, UserDetails userDetails) {
		final String username = extractUsername(token);
		return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
	}

	// ******----- Here we generates the token ------*****

	// Creating the token here
	private String createToken(Map<String, Object> claims, String subject) {
		return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
				.signWith(SignatureAlgorithm.HS256, secret).compact();
	}

	// Generating the token with using createToken function
	public String generateToken(String username, String role) {
		Map<String, Object> claims = new HashMap<>();
		claims.put("role", role);
		return createToken(claims, username);
	}
}
