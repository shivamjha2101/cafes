package com.example.cafe.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.cafe.model.UserData;
import com.example.cafe.repository.UserDataRepo;

@Component
@Service
public class CustomUserDetailsService implements UserDetailsService{
	@Autowired
	private UserDataRepo userRepo;

	private UserData userData;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		userData = userRepo.findByEmail(username);
		if (userData == null) {
			throw new UsernameNotFoundException("User Not found exception");
		}
		return new CustomUser(userData);
	}

	public UserData getUserDetail() {
		return userData;
	}
}
