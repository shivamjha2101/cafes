package com.example.cafe.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.example.cafe.Jwt.JwtFilter;
import com.example.cafe.constents.UserConstents;
import com.example.cafe.model.Category;
import com.example.cafe.repository.CategoryRepo;
import com.example.cafe.utils.CafeUtility;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepo categoryRepo;

	@Autowired
	private JwtFilter jwtFilter;

	@Override
	public ResponseEntity<String> addNewCategory(Category category) {
		// TODO Auto-generated method stub
		try {
			if (jwtFilter.isAdmin()) {
				categoryRepo.save(category);
				System.err.println(categoryRepo.save(category));
				return CafeUtility.getResponseEntity("Category added succesfully", HttpStatus.ACCEPTED);
			} else {
				return CafeUtility.getResponseEntity("Unauthorized access", HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<List<Object>> getAllCategory() {
		// TODO Auto-generated method stub
		try {
			return new ResponseEntity<List<Object>>(categoryRepo.getAllCategory(), HttpStatus.ACCEPTED);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return new ResponseEntity<>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<String> updateCategory(Category category) {
		try {
			Category updatedCategory = categoryRepo.findById(category.getId()).get();
			if (ObjectUtils.isEmpty(updatedCategory)) {
				return CafeUtility.getResponseEntity(UserConstents.INVALID_DATA, HttpStatus.BAD_REQUEST);
			} else {
				categoryRepo.save(category);
				return CafeUtility.getResponseEntity("Data updated succesfully", HttpStatus.ACCEPTED);
			}
		} catch (Exception ex) {
			return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
