package com.example.cafe.service;

import org.springframework.http.ResponseEntity;

import com.example.cafe.model.Bill;

public interface BillService {

	ResponseEntity<String> generateReport(Bill bill);
}
