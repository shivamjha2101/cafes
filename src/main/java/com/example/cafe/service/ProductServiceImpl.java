package com.example.cafe.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.example.cafe.Jwt.JwtFilter;
import com.example.cafe.constents.UserConstents;
import com.example.cafe.dto.ProductDto;
import com.example.cafe.model.Category;
import com.example.cafe.model.Product;
import com.example.cafe.repository.CategoryRepo;
import com.example.cafe.repository.ProductRepo;
import com.example.cafe.utils.CafeUtility;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepo productRepo;

	@Autowired
	private CategoryRepo categoryRepo;

	@Autowired
	private JwtFilter jwtFilter;

	@Override
	public ResponseEntity<String> addProduct(Product product) {
		// TODO Auto-generated method stub
		try {
			if (jwtFilter.isAdmin()) {
				Category category = categoryRepo.findById(product.getCategory().getId()).get();
				if (ObjectUtils.isEmpty(category)) {
					return CafeUtility.getResponseEntity(
							"The category with: " + product.getCategory().getId() + "Does not exist",
							HttpStatus.BAD_REQUEST);
				} else {
					product.setStatus("true");
					product.setCategory(category);
					productRepo.save(product);

					return CafeUtility.getResponseEntity("Product added succesfull", HttpStatus.ACCEPTED);
				}
			} else {
				return CafeUtility.getResponseEntity("Bad Request", HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<List<ProductDto>> getAllProduct() {
		// TODO Auto-generated method stub
		try {
			if (jwtFilter.isAdmin()) {
				return new ResponseEntity<List<ProductDto>>(productRepo.getAllProduct(), HttpStatus.ACCEPTED);
			} else {
				return new ResponseEntity<List<ProductDto>>(new ArrayList<>(), HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			return new ResponseEntity<List<ProductDto>>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<String> updateProduct(Product product) {
		// TODO Auto-generated method stub
		try {
			if (jwtFilter.isAdmin()) {
				if (productRepo.findById(product.getId()).get() == null) {
					return new ResponseEntity<String>("Product doesnot exist in database", HttpStatus.ACCEPTED);
				} else {
					product.setStatus("true");
					productRepo.save(product);
					return new ResponseEntity<String>("Product updated succesfully", HttpStatus.ACCEPTED);
				}

			} else {
				return new ResponseEntity<String>("You don't have right's to update products", HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<String> deleteProduct(Long id) {
		// TODO Auto-generated method stub
		try {
			if (jwtFilter.isAdmin()) {
				if (productRepo.findById(id).get() == null) {
					return new ResponseEntity<String>("Product doesnot exist in database", HttpStatus.BAD_REQUEST);
				} else {
					productRepo.deleteById(id);
					return new ResponseEntity<String>("Product deleted succesfully", HttpStatus.ACCEPTED);
				}
			} else {
				return new ResponseEntity<String>("You don't have right's to delete this products",
						HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
