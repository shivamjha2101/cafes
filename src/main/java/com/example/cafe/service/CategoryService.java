package com.example.cafe.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.example.cafe.model.Category;

public interface CategoryService {
	ResponseEntity<String> addNewCategory(Category category);

	ResponseEntity<List<Object>> getAllCategory();

	ResponseEntity<String> updateCategory(Category category);
}
