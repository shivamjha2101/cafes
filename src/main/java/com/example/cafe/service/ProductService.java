package com.example.cafe.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.example.cafe.dto.ProductDto;
import com.example.cafe.model.Product;

public interface ProductService {
	ResponseEntity<String> addProduct(Product product);

	ResponseEntity<List<ProductDto>> getAllProduct();

	ResponseEntity<String> updateProduct(Product product);

	ResponseEntity<String> deleteProduct(Long id);
}
