package com.example.cafe.service;

import java.io.FileOutputStream;
import java.util.Map;
import java.util.stream.Stream;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.web.server.ServerHttpSecurity.HttpsRedirectSpec;
import org.springframework.stereotype.Service;

import com.example.cafe.Jwt.JwtFilter;
import com.example.cafe.constents.UserConstents;
import com.example.cafe.model.Bill;
import com.example.cafe.utils.CafeUtility;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Service
public class BillServiceImpl implements BillService {

	@Autowired
	JwtFilter jwtFilter;

	@Override
	public ResponseEntity<String> generateReport(Bill bill) {
		// TODO Auto-generated method stub
		try {
			String filename;
			filename = CafeUtility.getUUid();
			bill.setCreatedBy(jwtFilter.getCurrentUser());
			String data = "Name: " + bill.getName() + "\n" + "Contact Number: " + bill.getContactNumber() + "\n"
					+ "Email: " + bill.getEmail() + "\n" + "Payment method: " + bill.getPaymentMethod();
			Document document = new Document();
			PdfWriter.getInstance(document,
					new FileOutputStream(UserConstents.STORE_LOCATION + "\\" + filename + ".pdf"));
			document.open();
			setRectangleInPdf(document);
			Paragraph chunk = new Paragraph("Cafe Management System: ", getFont("Header"));
			chunk.setAlignment(Element.ALIGN_CENTER);
			document.add(chunk);

			Paragraph para = new Paragraph(data + "\n \n", getFont("data"));
			document.add(para);

			PdfPTable pdfTable = new PdfPTable(5);
			pdfTable.setWidthPercentage(100);
			addTableHeader(pdfTable);

			JSONArray jsonArray = CafeUtility.getJsonArrayFromString((String) bill.getProductDetails());
			for (int i = 0; i < jsonArray.length(); i++) {
				addRows(pdfTable, CafeUtility.getMapFromJson(jsonArray.getString(i)));

			}
			document.add(pdfTable);
			document.close();
			return CafeUtility.getResponseEntity("{\"uuid\":\"" + filename + "}", HttpStatus.ACCEPTED);

		} catch (Exception e) {
			return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public void addRows(PdfPTable table, Map<String, Object> data) {
		table.addCell((String) data.get("name"));
		table.addCell((String) data.get("category"));
		table.addCell((String) data.get("quantity"));
		table.addCell(Double.toString((Double) data.get("price")));
		table.addCell(Double.toString((Double) data.get("total")));
	}

	public void setRectangleInPdf(Document document) throws DocumentException {
		Rectangle rect = new Rectangle(577, 825, 19, 15);
		rect.enableBorderSide(1);
		rect.enableBorderSide(2);
		rect.enableBorderSide(4);
		rect.enableBorderSide(8);
		rect.setBorderColor(BaseColor.BLACK);
		rect.setBorderWidth(1);
		document.add(rect);
	}

	public Font getFont(String header) {
		switch (header) {
		case "Header":
			Font headerFont = FontFactory.getFont(FontFactory.HELVETICA_BOLDOBLIQUE, 18);
			headerFont.setStyle(Font.BOLD);
			return headerFont;
		case "Data":
			Font dataFont = FontFactory.getFont(FontFactory.COURIER_BOLD, 12);
			dataFont.setStyle(Font.NORMAL);
			return dataFont;
		default:
			return new Font();
		}
	}

	public void addTableHeader(PdfPTable table) {
		Stream.of("Name", "Category", "Quantity", "Price", "Sub Total").forEach(columnTitle -> {
			PdfPCell header = new PdfPCell();
			header.setBackgroundColor(BaseColor.BLUE);
			header.setBorderWidth(2);
			header.setPhrase(new Phrase(columnTitle));
			header.setBackgroundColor(BaseColor.YELLOW);
			header.setHorizontalAlignment(Element.ALIGN_CENTER);
			header.setVerticalAlignment(Element.ALIGN_CENTER);
			table.addCell(header);
		});
	}
}
