package com.example.cafe.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.example.cafe.Jwt.JwtFilter;
import com.example.cafe.Jwt.JwtUtil;
import com.example.cafe.config.CustomUserDetailsService;
import com.example.cafe.constents.UserConstents;
import com.example.cafe.model.UserData;
import com.example.cafe.repository.UserDataRepo;
import com.example.cafe.utils.CafeUtility;
import com.example.cafe.utils.EmailUtils;
import com.google.common.base.Strings;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDataRepo userRepo;
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private CustomUserDetailsService customUserService;
	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private EmailUtils emailUtils;
	@Autowired
	private JwtFilter jwtFilter;

	@Override
	public ResponseEntity<String> signUp(UserData user) {
		// TODO Auto-generated method stub

		user.setRole("user");
		user.setStatus("false");
		user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
		userRepo.save(user);
		return CafeUtility.getResponseEntity("User registered succesfully", HttpStatus.ACCEPTED);
	}

	@Override
	public ResponseEntity<String> login(String username, String password) {
		// TODO Auto-generated method stub
		try {
			Authentication auth = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			if (auth.isAuthenticated()) {
				if (customUserService.getUserDetail().getStatus().equalsIgnoreCase("true")) {
					return new ResponseEntity<String>(
							"{\"token\":\"" + jwtUtil.generateToken(customUserService.getUserDetail().getEmail(),
									customUserService.getUserDetail().getRole()) + "\"}",
							HttpStatus.OK);
//					return CafeUtility.getResponseEntity("User logged in succesfully", HttpStatus.ACCEPTED);
				} else {
					return CafeUtility.getResponseEntity("Wait for admin approval", HttpStatus.BAD_REQUEST);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return CafeUtility.getResponseEntity("Bad Credentials", HttpStatus.BAD_GATEWAY);
	}

	@Override
	public ResponseEntity<List<Object>> getAllUser() {
		// TODO Auto-generated method stub
		try {
			if (jwtFilter.isAdmin()) {
				return new ResponseEntity<>(userRepo.getAllUser(), HttpStatus.ACCEPTED);
			} else {
				return new ResponseEntity<>(new ArrayList<>(), HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<String> update(UserData userData) {
		// TODO Auto-generated method stub
		try {
			if (jwtFilter.isAdmin()) {
				UserData updatedUser = userRepo.findById(userData.getId()).get();
				if (!ObjectUtils.isEmpty(updatedUser)) {
					String status = userData.getStatus();
					long id = userData.getId();
					Object o = userRepo.updateStatus(status, id);
					return CafeUtility.getResponseEntity("User status updated succesfully" + o, HttpStatus.ACCEPTED);
				} else {
					return CafeUtility.getResponseEntity("User id doest not exists in database",
							HttpStatus.UNAUTHORIZED);
				}
			} else {
				return CafeUtility.getResponseEntity(UserConstents.UNAUTHORIZED_ACCESS, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@Override
	public ResponseEntity<String> forgotPassword(String email) {
		// TODO Auto-generated method stub
		try {
			UserData userData = userRepo.findByEmail(email);
			if (ObjectUtils.isEmpty(userData) && !Strings.isNullOrEmpty(userData.getEmail())) {
				emailUtils.forgotMail(userData.getEmail(), "credentials by cafe management system",
						userData.getPassword());
				return CafeUtility.getResponseEntity("Check your mail for credentials", HttpStatus.ACCEPTED);
			} else {
				return CafeUtility.getResponseEntity(UserConstents.UNAUTHORIZED_ACCESS, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception ex) {
			return CafeUtility.getResponseEntity(UserConstents.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
