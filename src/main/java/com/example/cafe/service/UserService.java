package com.example.cafe.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.example.cafe.model.UserData;

public interface UserService {
	ResponseEntity<String> signUp(UserData user);

	ResponseEntity<String> login(String username, String password);

	ResponseEntity<List<Object>> getAllUser();

	ResponseEntity<String> update(UserData userData);

	ResponseEntity<String> forgotPassword(String email);

}
