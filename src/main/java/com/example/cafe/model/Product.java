package com.example.cafe.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQuery(name="Product.getAllProduct",query = "select new com.example.cafe.dto.ProductDto(p.id,p.name,p.description,p.price,p.status,p.category.id,p.category.name) from Product p")
@Entity
@Table(name="product")
public class Product {
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name="id")
     private long id;
     
     @Column(name="name")
     private String name;
     
     @ManyToOne(fetch=FetchType.LAZY)
     @JoinColumn(name="category_fk",nullable = false)
     private Category category;
     
     @Column(name="description")
     private String description;
     
     @Column(name="price")
     private int price;
     
     @Column(name="status")
     private String status;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", category=" + category + ", description=" + description
				+ ", price=" + price + ", status=" + status + "]";
	}
     
     
}
